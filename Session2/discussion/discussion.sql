-- Connection to MySQL via Terminal: mysql -u root
	-- "-u" : username
	-- "root" : defaul username for sql
	-- "-p" : password
	-- An empty value is the default pasword for SQL.

-- list down the db inside DBMS
SHOW DATABASES;

-- Note:
	-- SQL QUERIES is case insentive, but to easily identify the queries we usually use UPPERCASE
	-- Make sure to use (;) semi-colon at the end of sql syntax

-- create a database
CREATE DATABASE music_db;
	-- Naming convention is snake case

-- remove a specific database
DROP DATABASE music_db;

-- select the database
USE music_db;

-- create tables
	-- table columns' format: [column_name] [data_type] [other_options]
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50),
	PRIMARY KEY (id)
);

-- show tables
SHOW TABLES;
DESCRIBE USERS;

/*
    mini-activity:
        1. create a table for artists
        2. artists should have an id
        3. artists is required to have a name with 50 character limits
        4. assign the primary key to its id
        5. send a screenshot of your terminal in the batch space.
*/

-- Create table for artist
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

-- tables with foreign keys;
-- applying constraints in a table
	-- syntax:
		/*
			CONSTRAINT foreign_key_name
        		FOREIGN KEY (column_name)
         		REFERENCES table_name(id)
         		ON UPDATE ACTION
         		ON DELETE ACTION
		*/

-- When to create a constraints (foreign key)?
	-- If the relationship is one-to-one, "primary key" of the parent row is added as "foreign key" of the child row.
	-- If the relationship is one-to-many, "foreign key" column is added in the "many" entity/table.
	-- If the relationship is "many-to-many", linking table is created to contain the "foreign key" for both tables/entities

-- Create albums table
CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Date and time format
	-- Date: YYYY-MM-DD
	-- Time: HH:MM:SS
	-- Datetime: YYYY-MM-DD HH:MM:SS

-- Create songs table
CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_title VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT		
);

-- Create playlist table
CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	datetime_created DATETIME NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Create a joining playlists songs table
CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlist_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);
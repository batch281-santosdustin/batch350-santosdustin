/*

	1. Given the following ERD, create a new s02 and activity folder and then create a file named solution.sql and write the SQL code necessary to create the shown tables and images.
	2. Create a blog_db database.
	Note: Also include the primary key and foreign key constraints in your answer.
	3. Push your code in your gitlab repository and linked it to your Boodle.

*/

CREATE DATABASE blog_db;
USE blog_db;

-- users table
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

-- posts table
CREATE TABLE posts(
	id INT NOT NULL AUTO_INCREMENT,
	author_id INT NOT NULL,
	title VARCHAR(500) NOT NULL,
	content VARCHAR(5000) NOT NULL,
	datetime_posted DATETIME NOT NULL,
	PRIMARY key (id),
	CONSTRAINT fk_posts_author_id
		FOREIGN KEY (author_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- post_likes table
CREATE TABLE posts_likes(
	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_likes_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_posts_likes_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- post_comments table
CREATE TABLE posts_comments(
	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	content VARCHAR(5000),
	datetime_commented DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_comments_post_id
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_posts_comments_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT	
);
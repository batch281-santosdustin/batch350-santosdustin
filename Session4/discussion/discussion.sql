-- MYSQL ADVANCED SELECT QUERIES AND JOINS

-- To update increment
ALTER TABLE table_name AUTO_INCREMENT = 1;

-- [SECTION] DISCUSSION PREPARATION
--We will add 5 artists, atleast 2 albums each, 1-2 songs per album to expand the data that we can use.

-- Add five artists
	-- Taylor Swift
	-- Lady Gaga
	-- Justine Bieber
	-- Ariana Grande
	-- Bruno Mars

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justine Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Check if artists are added and what are their id:
SELECT * FROM artists;

-- Insert the following records in their respective tables
    -- Artist: Taylor Swift
        -- Album: Fearless 2008-11-11
            -- Songs:
                -- Fearless, 402, "Pop Rock"
                -- Love Story, 355, "Country Pop"
        -- Album: Red, 2012-10-22
            -- Songs:
                -- State of Grace, 455, "Rock, Alternative Rock, Arena Rock"
                -- Red, 341, "Country"

    -- Artist: Lady Gaga
        -- Album: A Star Is Born, 2018-10-05
            -- Songs:
                -- Black Eyes, 304, "Rock and Roll"
                -- Shallow, 336, "Country, Rock, Folk Rock"
        -- Album: Born This Way, 2011-05-23
            -- Song: Born This Way, 420, "Electropop"

    -- Artist: Justin Bieber
        -- Album: Purpose, 2015-11-13
            -- Song: Sorry, 320, "Dancehall-poptropical Housemoombahton"
        -- Album: Believe, 2012-06-15
            -- Song: Boyfriend, 252, "Pop"

    -- Artist: Ariana Grande
        -- Album: Dangerous Woman, 2016-05-20
            -- Song: Into You, 405, "EDM House"
        -- Album: Thank U, Next, 2019-02-08
            -- Song: Thank U, Next, 327, "Pop, R&B"

    -- Artist: Bruno Mars
        -- Album: 24k Magic, 2016-11-18
            -- Song: 24k Magic, 346, "Funk, Disco, R&B"
        -- ALbum: Earth to Mars,2011-02-07
            -- Song: Lost, 321, "Pop"

-- Taylor Swift Albums and Songs
-- Albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 3);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-22", 3);

-- Check what the id of the new albums:
SELECT * FROM albums;

-- Songs
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Fearless", 402, "Pop Rock", 3);
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Love Story", 355, "Country Pop", 3);
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("State of Grace", 455, "Rock, Alternative Rock, Arena Rock", 4);
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Red", 341, "Country", 4);

-- Check if the songs are added
SELECT * FROM songs;

-- Lady Gaga Albums and Songs
-- Albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-05", 4);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-05-23", 4);

-- Check what the id of the new albums:
SELECT * FROM albums;

-- Songs
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Black Eyes", 304, "Rock and roll", 5);
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Shallow", 336, "Country, Rock, Folk Rock", 5);
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Born This Way", 420, "Electropop", 6);

-- Check if the songs are added
SELECT * FROM songs;

-- Justin Bieber Albums and Songs
-- Albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 5);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-06-15", 5);

-- Check what the id of the new albums:
SELECT * FROM albums;

-- Songs
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Sorry", 320, "Dancehall-poptropical Housemoombahton", 7);
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Boyfriend", 252, "Pop", 8);

-- Check if the songs are added
SELECT * FROM songs;

-- Ariana Grande Albums and Songs
-- Albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-05-20", 6);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-02-08", 6);

-- Check what the id of the new albums:
SELECT * FROM albums;

-- Songs
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Into You", 405, "EDM House", 9);
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Thank U Next", 327, "Pop, R&B", 10);

-- Check if the songs are added
SELECT * FROM songs;

-- Bruno Mars Albums and Songs
-- Albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-11-18", 7);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-02-07", 7);

-- Check what the id of the new albums:
SELECT * FROM albums;

-- Songs
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("24K Magic", 346, "Funk, Disco, R&B", 11);
INSERT INTO songs (song_title, length, genre, album_id) VALUES ("Lost", 321, "Pop", 12);

-- Check if the songs are added
SELECT * FROM songs;

-- [SECTION] Advanced Selects

-- 1. Show only specific number of records [LIMIT]
	-- Syntax: SELECT * FROM songs LIMIT n;
SELECT * FROM songs LIMIT 5;

	-- Combine LIMIT with WHERE clause
SELECT * FROM songs WHERE length >= 430 LIMIT 5;

-- 2. Exclude a record [!]
	-- Syntax: SELECT * FROM songs WHERE album_id != n;
SELECT * FROM songs WHERE album_id != 11;

-- [ Mini - activity]
-- Exclude all songs from Lady Gaga
SELECT * FROM songs WHERE album_id != 5 AND album_id != 6;

-- 3. Finding records using comparison operator.
	-- >, <, >=, <=
SELECT * FROM songs WHERE length > 330;

-- 4. Getting records with multiple conditions [IN clause]
	-- IN : shorthand for multiple OR condition.
	-- Syntax: SELECT * FROM songs WHERE genre IN ("value1", "value2");
SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop"); -- Short for: SELECT * FROM songs WHERE genre = "Pop" OR genre = "K-Pop";

-- 5. Show records with a partial match [LIKE clause]
	-- LIKE : used in a WHERE clause to search for a specified pattern in a column.
	-- There are 2 wildcard operators used in conjuction with LIKE.
		-- % : [PERCENT] which represents zero, one, or multiple characters
			-- Can be xx%, %xx, %xx%
		-- _ : [UNDERSCORE] which represents a single character
		-- Syntax: SELECT * FROM songs WHERE song_name LIKE "condition%";
SELECT * FROM songs WHERE song_title LIKE "th%";

SELECT * FROM songs WHERE song_title LIKE "__rr_";
SELECT * FROM albums WHERE date_released LIKE "20__-0_-__";

-- Find values w/ a match at certain positions
SELECT * FROM albums WHERE album_title LIKE "_ur%";
SELECT * FROM albums WHERE album_title LIKE "%_r%";

-- 6. Sorting records [ORDER BY]
	-- ORDER BY : used to sort the results to ascending (ASC) or descending (DESC) order
	-- Syntax: SELECT * FROM table_name ORDER BY column_name ASC/DESC;
SELECT * FROM songs ORDER BY song_title;
SELECT * FROM songs ORDER BY song_title ASC;
SELECT * FROM songs ORDER BY song_title DESC;

SELECT * FROM songs WHERE album_id != 5 AND album_id != 11 ORDER BY song_title ASC;

-- 7. Showing records with distinct values [DISTINCT]
	-- Syntax: SELECT DISTINCT column_name FROM table_name;
SELECT DISTINCT genre FROM songs;

-- 8. Count the number of rows in the table
	-- Syntax: SELECT COUNT(column_name) FROM table_name;
SELECT COUNT(*) FROM songs;
SELECT COUNT(DISTINCT genre) FROM songs; 

-- [SECTION] Table joins
-- To retrieve data from multiple tables

-- 1. Inner Join (join)
	-- Syntax: SELECT column_name FROM table1 JOIN table2 ON table1.id = table2.foreign_key_column;
SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id;
SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id WHERE date_released <= 20151201;

-- 2. Joining multiple talbes
	-- Syntax: SELECT column_name FROM table1
		-- JOIN table2 ON table1.id = table2.foreign_key_column
		-- JOIN table3 ON table2.id = table3.foreign_key_column;
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT name, album_title, song_title, length FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- 3. Provide aliases for joining table
SELECT name AS "Artist name", album_title AS Album, song_title AS Song, length AS Duration, genre AS Genre
	FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;	
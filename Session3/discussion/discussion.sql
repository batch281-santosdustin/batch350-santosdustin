-- MySQL CRUD Operation

-- [SECTION] Inserting a record

-- Inserting a single record
	-- Syntax: INSERT INTO table_name (column_name) VALUES (Value1);
INSERT INTO artists (name) VALUES ("Psy");
INSERT INTO artists (name) VALUES ("Rivermaya");

-- Inserting record w/ multiple columns
	-- Syntax: INSERT INTO table_name (column_name1, column_name2, ... column_nameN) VALUES (value1, value2, ... valueN);
		-- follow order of column & value

	-- Albums table
INSERT INTO albums (album_title, date_released, artist_id) VALUE ("Psy 6", "2013-08-15", "1");
INSERT INTO albums (album_title, date_released, artist_id) VALUE ("Trip", "1996-02-14", "2");

	-- Songs table
INSERT INTO songs (song_title, length, genre, album_id) VALUE ("Gangnam Style", 339, "Pop", "1");

-- Mini-Activity:
	-- Insert the following songs in the songs table:

		-- Song name: Kundiman
		-- Length: 5 mins 24 secs
		-- Genre: OPM
		-- Album: Trip

		-- Song name: Kisapmata
		-- Length: 4 mins 41 secs
		-- Genre: OPM
		-- Album: Trip

	-- Check if the songs are added.
	-- send your songs table in the batch hangouts
INSERT INTO songs (song_title, length, genre, album_id) VALUE ("Kundiman", 524, "OPM", "2");
INSERT INTO songs (song_title, length, genre, album_id) VALUE ("Kisapmata", 441, "OPM", "2");

-- Adding multiple records with one query
	-- Syntax: INSERT INTO table_name (column_name1, column_name2) VALUES (value1a, value2a), (value1b, value2b)
INSERT INTO songs (song_title, length, genre, album_id) VALUE ("Kundiman", 524, "OPM", "2"), ("Kisapmata", 441, "OPM", "2");

-- [SECTION] Selecting records

-- Showing all record details
	-- Syntax: SELECT * FROM table_name;
	-- * : all
SELECT * FROM songs;

-- Show recrods w/ selected columns.
	-- Syntax: SELECT column_nameA, column_nameB FROM table_name;
SELECT song_title, genre FROM songs;
SELECT album_title, date_released FROM albums;

-- Show records that meets a certain condition
	-- Syntax: SELECT column_name FROM table_name WHERE condition;
SELECT song_title FROM songs WHERE genre = "OPM";
SELECT song_title FROM songs WHERE length < 400; -- or "00:04:00"

-- Show records w/ multiple conditions
	-- Syntax: [AND CLAUSE] SELECT column_name FROM table_name WHERE condition1 AND condition2;
SELECT song_title, length FROM songs WHERE length > 430 AND genre = "OPM";

-- [SECTION] Updating records

-- Updating single column of a record
	-- Syntax: UPDATE table_name SET column_name = new_value WHERE condition;
UPDATE songs SET length = 424 WHERE song_title = "Kundiman";

-- Updating multiple columns of records
	-- Syntax: UPDATE table_name SET column_name1 = value1, column_name2 = value2 WHERE condition;
UPDATE albums SET album_title = "Psy 6 (Six Rules)", date_released = 20120715 WHERE album_title = "Psy 6";

-- [SECTION] Deleting records

-- Deleting a record
	-- Syntax: DELETE FROM table_name WHERE condition;
	-- Remember: Removing the WHERE clause will remove all rows in the table
DELETE FROM songs WHERE genre = "OPM" AND length > 430;
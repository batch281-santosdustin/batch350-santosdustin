/*
	1. Create a s03 and activity folder and then create a file named solution.sql and write the SQL code necessary to insert the following records in the tables and to perform the following query.
	2. Add the following records to the blog_db database:
	    - Check User Image
	    - Check Posts Image
	3. Get all the post with an Author ID of 1.
	4. Get all the user's email and datetime of creation.
	5. Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.
	6. Delete the user with an email of "johndoe@gmail.com".
	7. Push your solution in your gitlab repository and linked it to your Boodle.
*/

-- [SECTION] Add records
-- Add users
INSERT INTO users (email, password, datetime_created) VALUE ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUE ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUE ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUE ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUE ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- Add posts
INSERT INTO posts (author_id, title, content, datetime_posted) VALUE (1, "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUE (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUE (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUE (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- [SECTION] Get all the post with an Author ID of 1
SELECT title, content FROM posts WHERE author_id = 1;

-- [SECTION] Get all the user's email and datetime of creation
SELECT email, datetime_created FROM users;

-- [SECTION] Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- [SECTION] Delete the user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";

-- [SECTION] Stretch goals
/*
	1. Insert the following records in the music_db database.
	    USER:
	        username: johndoe
	        password: john1234
	        full name: John Doe
	        contact number: 09123456789
	        email: john@mail.com
	        address: Quezon City

	    PLAYLIST:
	        johndoe's playlist
	        created at: September 20, 2023 8:00 AM

	    PLAYLIST SONG:
	        Gangnam Style
	        Kundiman

	    Note: Make sure to display each table to check if the record is added.

	2. Write the SQL code necessary to insert the following records in the solution.sql.
*/

-- Add user
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("johndoe", "john1234", "John Doe", "09123456789", "john@mail.com", "Quezon City");

-- Add playlist
INSERT INTO playlists (datetime_created, user_id) VALUES ("2023-09-20 8:00:00", 1);

-- Add playlist song
INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 1);
INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 2);